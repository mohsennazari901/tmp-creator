import qprompt
import datetime
import git
import configparser
import gitlab
import colorama
from termcolor import colored

colorama.init()


def config(key, scope="repository", default=None):
    cfg_parser = configparser.ConfigParser()
    cfg_parser.read(git_config_path)
    if scope not in cfg_parser or key not in cfg_parser[scope] or cfg_parser[scope][key] == '':
        return default
    return cfg_parser[scope][key]


git_config_path = './.python-gitlab.cfg'
gitPath = config("git_path", default='.')

remote_origin = config("remote_origin")
origin_name = config("origin_name")
project_name = config("project_name")
merge_requests_label = config("merge_requests_label")
default_branch = config("default_branch")


def is_remote_branch(name):
    gitInstance = git.Git(gitPath)
    return len(gitInstance.execute([
        "git",
        "ls-remote",
        "--heads",
        remote_origin,
        name
    ])) > 0


def is_local_branch(name):
    repo = git.Repo(gitPath)

    return name in repo.branches


def get_today_temp():
    currentDay = datetime.date.today()
    return f"tmp/{currentDay.strftime('%b').lower()}-{currentDay.strftime('%d')}-stage-mrs"


def update_temp_branch(temp_name):
    gitInstance = git.Git(gitPath)
    pull_temp = qprompt.ask_yesno("Do you want to pull changes?", default='n')
    if pull_temp:
        gitInstance.fetch(origin_name, temp_name, temp_name)
    pull_default_branch = qprompt.ask_yesno("Do you want to pull from default branch?", default='n')
    if pull_default_branch:
        gitInstance.fetch(origin_name, default_branch, temp_name)


def push_to_remote(name):
    gitInstance = git.Git(gitPath)
    gitInstance.execute(['git', 'push', origin_name, f"refs/heads/{name}:{name}"])
    "git push origin refs/heads/tmp/aug-21-stage-mrs:tmp/aug-21-stage-mrs"


def identify_merge_fail_reason(issues):
    issues_str = "".join(issues)

    if "Merge conflict" in issues_str:
        return "Merge Conflict"
    else:
        return issues_str


def merge_branches(to, branches, update_selected_branches=True):
    repo = git.Repo(gitPath)
    g = git.Git()
    g.fetch(origin_name)
    merged_branches = []
    branches_with_issues = []
    for branch in branches:
        try:
            if is_local_branch(branch):
                repo.git.checkout(branch)
            elif is_remote_branch(branch):
                g.execute(['git', 'checkout', "-b", branch, f"{origin_name}/{branch}"])
        except:
            pass

        if update_selected_branches:
            try:
                repo.git.merge(origin_name + "/" + default_branch)
            except:
                repo.git.merge(abort=True)

    repo.git.checkout(to)

    for branch in branches:
        try:
            repo.git.merge("origin/" + branch)
            merged_branches.append(branch)
        except Exception as e:
            try:
                repo.git.merge(abort=True)
            except:
                pass
            branches_with_issues.append({
                'branch': branch,
                'issue': str(e)
            })
            continue

    print("Merged branches:")
    print_branches(merged_branches, 'green')
    print("*" * 60, "\nSkipped branches:")
    for i, b in enumerate(branches_with_issues):
        text = f"\t {i:2}) {identify_merge_fail_reason(b['issue'])}: {b['branch']}"
        print(colored(text, 'red'))

    return [merged_branches, branches_with_issues]


def print_branches(branches, color='white'):
    for i, b in enumerate(branches):
        text = f"\t {i:2}) {b}"
        print(colored(text, color))


def merge_to_temp_menu(branches):
    branches_to_merge = branches.copy()
    qprompt.clear()
    menu = qprompt.Menu()
    menu.add("a", "Select All")
    menu.add("n", "Select None")
    menu.add("s", "Select one")
    menu.add("r", "Remove One")
    menu.add("c", "Continue to Merge")
    while 1:
        print("Branches are:")
        for i, b in enumerate(branches):
            text = f"\t {i:2}) {b}"
            color = "green" if b in branches_to_merge else "white"
            print(colored(text, color))

        choice = menu.show()
        if choice == 'a':
            branches_to_merge = branches.copy()
        if choice == 'n':
            branches_to_merge = []
        if choice == 's':
            selected_branch_index = qprompt.ask_int("Branch index?")
            selected_branch = branches[selected_branch_index]
            branches_to_merge.append(selected_branch)
        if choice == 'r':
            selected_branch_index = qprompt.ask_int("Branch index?")
            selected_branch = branches[selected_branch_index]
            branches_to_merge.remove(selected_branch)
        if choice == 'c':
            break
        qprompt.clear()
        import os
        os.system("clear")

    return branches_to_merge


def merge_branches_to_temp(temp_name):
    merge_brs = qprompt.ask_yesno("Do you want to merge branches?", default='y')
    if merge_brs:
        target_branches = get_target_branches()
        if len(target_branches) == 0:
            push_to_remote(temp_name)
            qprompt.warn("Nothing to merge")
            return
        branches_to_merge = merge_to_temp_menu(target_branches)
        merge_to_temp = qprompt.ask_yesno("Do you want to proceed with merge?", default='y')
        if merge_to_temp:
            update_selected_branches = qprompt.ask_yesno("Do you want to update selected branches?", default='n')
            merge_branches(temp_name, branches_to_merge, update_selected_branches)


def get_remote_branches():
    repo = git.Repo(gitPath)
    remote_branches = []
    for ref in repo.git.branch('-r').split('\n'):
        remote_branches.append(ref.strip())
    return remote_branches


def get_latest_temp():
    rbs = get_remote_branches()
    latest = None
    latestDate = None
    for rb in rbs:
        if "tmp/" in rb:
            rb_name = rb.split('/')[-1]
            rb_date = rb_name.split('-')[0:2]
            try:
                date_time_obj = datetime.datetime.strptime('-'.join(rb_date), '%b-%d')
            except:
                continue
            if latest is None:
                latest = rb
                latestDate = date_time_obj
            else:
                if date_time_obj > latestDate:
                    latest = rb
                    latestDate = date_time_obj

    latest = '/'.join(latest.split('/')[1:])
    return latest


def start_new_temp():
    tmp_default = get_today_temp()
    temp_name = qprompt.ask_str("Temp name:", tmp_default)
    if is_remote_branch(temp_name):
        qprompt.warn("This remote branch already exists!")
        update_temp_branch(temp_name)
    elif is_local_branch(temp_name):
        qprompt.warn("This local branch already exists!")
        push_to_remote(temp_name)
    else:
        gitInstance = git.Git(gitPath)
        gitInstance.execute(['git', 'checkout', "-b", temp_name, f"{origin_name}/{default_branch}"])
        gitInstance.execute(['git', 'push', '-u', "origin", temp_name])

    merge_branches_to_temp(temp_name)
    push_merges = qprompt.ask_yesno("Do you want to push merges?", default='n')
    if push_merges:
        push_to_remote(temp_name)


def update_latest_temp():
    latest_temp = get_latest_temp()
    temp_name = qprompt.ask_str("Latest temp name:", latest_temp)
    update_temp_branch(temp_name)

    merge_branches_to_temp(temp_name)
    push_merges = qprompt.ask_yesno("Do you want to push merges?", default='n')
    if push_merges:
        push_to_remote(temp_name)


def get_target_branches():
    gl = gitlab.Gitlab.from_config('core', [git_config_path])
    project = gl.projects.get(project_name)
    merge_requests = project.mergerequests.list(state='opened', labels=merge_requests_label,
                                                target_branch=default_branch, get_all=True)
    to_merge = []
    for mr in merge_requests:
        # todo: maybe we need to merge Drafts too
        if "Draft" not in mr.labels:
            to_merge.append(mr.source_branch)

    return to_merge


def show_menu():
    qprompt.clear()
    menu = qprompt.Menu()
    menu.add("n", "New temp")
    # menu.add("u", "Update latest temp")
    menu.add("q", "Quit")
    while 1:
        choice = menu.show()
        if choice == 'n':
            start_new_temp()
        # if choice == 'u':
        #     update_latest_temp()
        if choice == 'q':
            exit(0)
        qprompt.pause()
        qprompt.clear()
        import os
        os.system("clear")


if __name__ == '__main__':
    show_menu()
