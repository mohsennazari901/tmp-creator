# Temp Builder
This script is used for creating stage branch groups as a part of automating making multi-stage environment.  

## Install
First clone the project to a directory with the same access as your `snappfood-core` project.

then install `python >= 3.9` with pip and virtualenv.
```shell
sudo apt install python3-virtualenv
```
then create and activate it inside project dir based on your os (this is for ubuntu):
```shell
virtualenv venv && source venv/bin/activate
```
then run:
```shell
pip install -r requirements.txt
```

## Usage:
First copy `.python-gitlab.cfg.example` to `.python-gitlab.cfg` and change 
the values, specially `private_token` of your gitlab access, and `git_path` 
pointing to your core project root.

Just run the interactive shell
```shell
python run.py
```